package com.facundoruiz.interviews.challenges.mulesoft.commands;

import com.facundoruiz.interviews.challenges.mulesoft.filesystem.FileSystem;

public interface Command{
    void run(FileSystem fileSystem);
}
