package com.facundoruiz.interviews.challenges.mulesoft.filesystem;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Folder extends FileSystemNode{

    Map<String, FileSystemNode> nodes = new HashMap<>();

    public Folder(String name, Folder node){
        super(name, node);
    }

    public void addFile(String fileName) throws NodeCreationException {

        if(nodes.containsKey(fileName)){
            throw new NodeCreationException();
        }
        nodes.put(fileName, new File(fileName, this));
    }

    public void addFolder(String folderName) throws NodeCreationException{
        if(nodes.containsKey(folderName)){
            throw new NodeCreationException();
        }
        nodes.put(folderName, new Folder(folderName, this));
    }

    public Boolean hasFolder(String folderName){

        Boolean response = false;
        if(has(folderName)){
            FileSystemNode node = nodes.get(folderName);
            if(node.isFolder()){
                response = true;
            }
        }

        return response;

    }

    public Boolean hasFile(String fileName){

        Boolean response = false;
        if(has(fileName)){
            FileSystemNode node = nodes.get(fileName);
            if(node.isFile()){
                response = true;
            }
        }

        return response;

    }

    public Boolean has(String nodeName){

        Boolean response = false;
        if(nodes.containsKey(nodeName)){
            response = true;
        }

        return response;

    }

    public Folder getFolder(String folderName) throws DirectoryNotFoundException {
        if(folderName == "."){
            return this;
        }else if(folderName == ".."){
            return this.parent;
        }
        FileSystemNode node = nodes.get(folderName);

        if(node == null){
            throw new DirectoryNotFoundException();
        }

        if(node.isFolder()){
            return (Folder) node;
        }
        throw new DirectoryNotFoundException();
    }

    public File getFile(String fileName){
        FileSystemNode node = nodes.get(fileName);
        if(node.isFile()){
            return (File) node;
        }
        return null;
    }

    public FileSystemNode get(String folderName){
        return nodes.get(folderName);
    }

    public Boolean isFolder(){
        return true;
    }

    public Set<String> list(){
        return nodes.keySet();
    }

}
