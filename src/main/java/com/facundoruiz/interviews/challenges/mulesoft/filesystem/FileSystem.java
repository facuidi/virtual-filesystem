package com.facundoruiz.interviews.challenges.mulesoft.filesystem;

import java.util.Set;

final public class FileSystem extends FileSystemNode {

    final Folder root;
    final String name = "/root";
    Folder currentDirectory;

    public FileSystem(){
        root = new Folder(name, null);
        currentDirectory = root;
    }

    public Folder getRoot(){
        return root;
    }

    public String getPath(){
        return name;
    }

    public String getName() {
        return name;
    }

    public Folder getCurrentDirectory() {
        return currentDirectory;
    }

    public void changeDirectory(String directoryName) throws DirectoryNotFoundException{
        if(directoryName.equals("")){
            currentDirectory = root;
        }else if(directoryName.equals(".")){
            //does nothing
        }else if(directoryName.equals("..")){
            if(currentDirectory.getParent() != null) {
                currentDirectory = currentDirectory.getParent();
            }
        }else{
            if(currentDirectory.hasFolder(directoryName)){
                currentDirectory = currentDirectory.getFolder(directoryName);
            }else{
                throw new DirectoryNotFoundException();
            }
        }
    }

    public void createDirectory(String name) throws NodeCreationException {
        if(!currentDirectory.has(name)){
            currentDirectory.addFolder(name);
        }
    }

    public void createFile(String name) throws NodeCreationException {
        if(!currentDirectory.has(name)){
            currentDirectory.addFile(name);
        }
    }

    public Set<String> list(){
        return currentDirectory.list();
    }


}
