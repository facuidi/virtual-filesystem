package com.facundoruiz.interviews.challenges.mulesoft.commands;

import com.facundoruiz.interviews.challenges.mulesoft.filesystem.DirectoryNotFoundException;
import com.facundoruiz.interviews.challenges.mulesoft.filesystem.FileSystem;

import java.util.List;

class ChangeDirectoryCommand implements Command{
    List<String> parameters;
    public ChangeDirectoryCommand(List<String> parameters){
        this.parameters = parameters;
    };

    public void run(FileSystem fileSystem){
        if(parameters.size() != 1){
            System.out.println("Invalid Command");
            return;
        }

        try {
            fileSystem.changeDirectory(parameters.get(0));
        } catch (DirectoryNotFoundException e) {
//            e.printStackTrace();
            System.out.println("Directory not found");
        }
    }
}
