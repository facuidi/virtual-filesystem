package com.facundoruiz.interviews.challenges.mulesoft.commands;

import com.facundoruiz.interviews.challenges.mulesoft.filesystem.FileSystem;
import com.facundoruiz.interviews.challenges.mulesoft.filesystem.Folder;
import com.facundoruiz.interviews.challenges.mulesoft.filesystem.NodeCreationException;

import java.util.List;

class MakeDirectoryCommand implements Command{

    List<String> parameters;
    public MakeDirectoryCommand(List<String> parameters){
        this.parameters = parameters;
    };

    public void run(FileSystem fileSystem){

        Folder currentDirectory = fileSystem.getCurrentDirectory();

        if(parameters.size() == 0){
            System.out.println("Invalid Command");
            return;
        }

        for(String folderName: parameters){

            if(folderName.length() > 100){
                System.out.println("Invalid File or Folder Name");
                continue;
            }else if(currentDirectory.has(folderName)){
                System.out.println("File or Directory Already exists");
                continue;
            }

            try {
                fileSystem.createDirectory(folderName);
            } catch (NodeCreationException e) {
                e.printStackTrace();
            }

        }
    }
}
