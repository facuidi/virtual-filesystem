package com.facundoruiz.interviews.challenges.mulesoft.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CommandLine{
    private String commandName;
    private List<String> parameters = new ArrayList<>();

    public CommandLine(String line){

        List<String> commandLine = Arrays.asList(line.split("\\s"))
        .stream().filter(str -> !str.equals("")).collect(Collectors.toList());

        commandName = commandLine.get(0);

        if(commandLine.size() >1){
            parameters = commandLine.subList(1, commandLine.size());
        }

    };

    public String getCommandName(){
        return this.commandName;
    }

    public List<String> getParameters(){
        return  parameters;
    }

    public String toString(){
        return "Command Name: " + commandName + ", Parameters: " + parameters;
    }


}
