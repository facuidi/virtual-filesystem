package com.facundoruiz.interviews.challenges.mulesoft.filesystem;

public class File extends FileSystemNode {

    public File(String name, Folder node){
        super(name, node);
    }

    public Boolean isFile(){
        return true;
    }
}
