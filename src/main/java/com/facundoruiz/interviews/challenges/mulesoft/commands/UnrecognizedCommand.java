package com.facundoruiz.interviews.challenges.mulesoft.commands;

import com.facundoruiz.interviews.challenges.mulesoft.filesystem.FileSystem;

class UnrecognizedCommand implements Command{
    public void run(FileSystem fileSystem){
        System.out.println("Unrecognized Command");
    }
}
