package com.facundoruiz.interviews.challenges.mulesoft.commands;

import com.facundoruiz.interviews.challenges.mulesoft.filesystem.FileSystem;

public class QuitCommand implements Command{
    public void run(FileSystem fileSystem){}
}
