package com.facundoruiz.interviews.challenges.mulesoft.filesystem;

abstract class FileSystemNode{

    private final String separator = "/";

    Folder parent;
    String name;


    public FileSystemNode(){}
    public FileSystemNode(String name, Folder parent){
        this.name = name;
        this.parent = parent;
    }

    public Folder getParent(){
        return parent;
    }

    public String getPath(){
        return (parent == null)?name:parent.getPath() + separator + name;
    }

    public Boolean isFile(){
        return false;
    }

    public  Boolean isFolder(){
        return false;
    }

    public void addFile(String fileName) throws NodeCreationException {}

    public void addFolder(String fileName) throws NodeCreationException{}

}
