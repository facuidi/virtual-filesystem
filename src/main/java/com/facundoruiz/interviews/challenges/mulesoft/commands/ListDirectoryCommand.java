package com.facundoruiz.interviews.challenges.mulesoft.commands;

import com.facundoruiz.interviews.challenges.mulesoft.filesystem.DirectoryNotFoundException;
import com.facundoruiz.interviews.challenges.mulesoft.filesystem.FileSystem;
import com.facundoruiz.interviews.challenges.mulesoft.filesystem.Folder;

import java.util.List;

class ListDirectoryCommand implements Command{

    List<String> parameters;

    public ListDirectoryCommand(List<String> parameters){
        this.parameters = parameters;
    }

    public void run(FileSystem fileSystem){

        String directoryName = ".";

        if(parameters == null){
            System.out.println("Invalid Command" + parameters.size());
            return;
        }
        if(parameters.size() == 1){
            directoryName = parameters.get(0);
        }

        try {
            Folder directory = fileSystem.getCurrentDirectory().getFolder(directoryName);
            System.out.println(String.join(", ", directory.list()));
        } catch (DirectoryNotFoundException e) {
//            e.printStackTrace();
            System.out.println("Directory not found");
        }

    }
}
