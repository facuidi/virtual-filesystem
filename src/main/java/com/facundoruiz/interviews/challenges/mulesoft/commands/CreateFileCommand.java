package com.facundoruiz.interviews.challenges.mulesoft.commands;

import com.facundoruiz.interviews.challenges.mulesoft.filesystem.FileSystem;
import com.facundoruiz.interviews.challenges.mulesoft.filesystem.Folder;
import com.facundoruiz.interviews.challenges.mulesoft.filesystem.NodeCreationException;

import java.util.List;

class CreateFileCommand implements Command{
    List<String> parameters;

    public CreateFileCommand(List<String> parameters){
        this.parameters = parameters;
    };

    public void run(FileSystem fileSystem){

        Folder currentDirectory = fileSystem.getCurrentDirectory();

        for(String fileName: parameters){

            if(fileName.length() > 100){
                System.out.println("Invalid File or Folder Name");
                continue;
            }else if(currentDirectory.has(fileName)){
                System.out.println("File or Directory Already exists");
                continue;
            }

            try {
                fileSystem.createFile(fileName);
            } catch (NodeCreationException e) {
                e.printStackTrace();
            }

        }
    }

}
