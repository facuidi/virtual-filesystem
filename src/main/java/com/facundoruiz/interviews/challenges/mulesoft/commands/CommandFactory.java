package com.facundoruiz.interviews.challenges.mulesoft.commands;

import com.facundoruiz.interviews.challenges.mulesoft.*;

import java.util.List;

public class CommandFactory{

    private CommandFactory(){}

    static public Command createCommand(CommandLine commandLine) throws InvalidParametersException {
        Command returnedCommand = null;
        String commandName = commandLine.getCommandName();
        List<String> parameters = commandLine.getParameters();
        if(commandName.equals("quit")){
            returnedCommand = new QuitCommand();
        }else if(commandName.equals("ls")){
            returnedCommand = new ListDirectoryCommand(parameters);
        }else if(commandName.equals("pwd")){
            returnedCommand = new CurrentDirectoryCommand();
        }else if(commandName.equals("mkdir")){
            returnedCommand = new MakeDirectoryCommand(parameters);
        }else if(commandName.equals("cd")){
            returnedCommand = new ChangeDirectoryCommand(parameters);
        }else if(commandName.equals("touch")){
            returnedCommand = new CreateFileCommand(parameters);
        }else{
            returnedCommand = new UnrecognizedCommand();
        }
        return returnedCommand;
    }

}
