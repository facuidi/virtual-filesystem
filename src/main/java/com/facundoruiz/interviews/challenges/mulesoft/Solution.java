package com.facundoruiz.interviews.challenges.mulesoft;

import com.facundoruiz.interviews.challenges.mulesoft.commands.*;
import com.facundoruiz.interviews.challenges.mulesoft.filesystem.*;
import java.util.*;

public class Solution {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String args[] ) throws Exception {

        /* Enter your code here. Read input from STDIN. Print output to STDOUT */

        Boolean exit = false;

        FileSystem filesystem = new FileSystem();

        while(!exit){
            try{
                Command command = getCommand();
                command.run(filesystem);
                if(command instanceof QuitCommand){
                    exit = true;
                }
            // }catch(InvalidParametersException e){
            //     System.out.println(invalidCommand);
            // }catch(UnrecognizedCommandException e){
            //      System.out.println(unrecognizedCommand);
            }catch(NoSuchElementException e){
                exit = true;
            }
        }

    }

    private static CommandLine getCommandLine(){
        return new CommandLine(scanner.nextLine());
    }

    private static Command getCommand() throws InvalidParametersException {
        return CommandFactory.createCommand(getCommandLine());
    }

}
